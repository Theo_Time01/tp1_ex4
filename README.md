#  TP1 Exercice 4

## Objectif 
L'objectif de cet exercice est de créer des packages permettant et de permettre la communication entre eux.

## Realisation 
J'ai créé deux packages, un permettant de stocker le script de SimpleCalculator et un autre package contenant un script de test.

## Organisation
voici l'arborescence de notre projet :

    .
    ├── Package_Calculator
    │   ├── Calculator.py
    │   ├── __init__.py
    │   └── Calculator.pyc
    │  
    ├── Package_Test
    │   ├── exo4.py
    │   └── __init__.py
    └── README.md



## Lancement
Si on essaie d'éxécuter le script exo_4.py, il n'arrivera pas à trouver le package Package_Claculator.  
Il faut donc initialiser le PYHTONPATH à la racine du répertoire du projet pour qu'il puisse pointer sur les différents packages. Pour cela il faut entrer la commande :  
  
    export PYTHONPATH=$PYTHONPATH:'.'

puis on execute la commande :  

    python Package_Test/exo_4.py

