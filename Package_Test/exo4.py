#!/usr/bin/env python
# coding: utf-8
"""
author : theotime Perrichet

Ce code permet de tester à l'intérieur les méthodes 
d'une classe contenue dans un autre package
"""
#importation de la classe via le package_calculator
from Package_Calculator.Calculator import SimpleComplexCalculator


TEST = SimpleComplexCalculator([1, 2], [1, 3])  # crée une instance de SimpleComplexCalculator
print(
    "somme : ", TEST.sum_complex()
)  # affiche la chaine de caractères "somme : " et le résultat de la méthode sum_complex
print(
    "soustraction : ", TEST.substract_complex()
)  # affiche le résultat de la méthode substract_complex
print(
    "multiplication : ", TEST.multiply_complex()
)  # affiche le résultat de la méthode multiply_complex
print(
    "division : ", TEST.divide_complex()
)  # affiche le résultat de la méthode divide_complex
